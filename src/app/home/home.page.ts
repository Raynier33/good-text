import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import * as Clipboard from 'clipboard/dist/clipboard.min.js';
import { Validators, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { RecordService } from "../shared/record/record.service";


@Component({
  selector: 'app-home',
  templateUrl: 'home.html',
  styleUrls: ['home.scss']



})
export class HomePage {

//good text items
new_item_form: FormGroup;
 description: string;
 fsr: string;
 oop:string;
 fsrValue: string;
 fsrNumber = "N/A";
 departments: string;
 resolution: string;
 restocking: string = " N/A";
 sts: string = " N/A";
 goodText;
 clipboard;
 title = "Good Text";
 oopDetails: string = "";
goodTextForm: FormGroup;
descriptionForm: FormGroup;
errorForm: FormGroup;
resolutionForm: FormGroup;
restockingForm: FormGroup;
fsrForm: FormGroup;
stsForm:FormGroup;
oopDescription: string;
oopForm: FormGroup;
submitted = false;
lockSlide
//record
data:any;
badge: any;
newRecord: object ;
oopRecord
recordTime

//templates
oopTemplate = `1.Where did the error occur? / Who caused the error?\n2.What is the root cause of this return? What happened exactly?\n3.Why is this request being requested outside the 30 day policy? What caused the delay?`;
largeDollarTemplate = `1.What is the root cause of the return?\n2. Where did the error occur/Who caused the error/How exactly were the orders placed incorrectly?\n3.What efforts has been made to save the sale?\n4.When did the customer first contact Dell about this issue that is now resulting in the return request?\n4.Does the customer require Tech Support Assistance?`;


@ViewChild ('goodTextSlider') goodTextSlider: any;
@ViewChild('myInput') myInput: ElementRef;


// slides 

  constructor( public el:ElementRef, private keyboard: Keyboard, private fb: FormBuilder, private recordService: RecordService, private route: ActivatedRoute, private router: Router) {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.data = this.router.getCurrentNavigation().extras.state.agentId;
        this.badge = this.data[Object.keys(this.data)[0]]
        console.log(this.badge);
      }
    }); 
  }

  ngOnInit() {
   
    this.goodTextSlider.lockSwipes(true);
    this.new_item_form = this.fb.group({
      title: new FormControl('', Validators.required),     
    });  

    this.descriptionForm = this.fb.group({
      text: new FormControl('', Validators.required)
    });
    this.errorForm = this.fb.group({
      department: new FormControl('', Validators.required),  
    });
    this.resolutionForm = this.fb.group({
      action: ['', Validators.required]
    });
    this.restockingForm = this.fb.group({
      restOption: ['']
    });
    this.fsrForm = this.fb.group({
      fsrValue: ['']
    });
    this.stsForm = this.fb.group({
      ststOption: [''],

    });
     this.oopForm = this.fb.group({
      oopText: ['']
    });

  }




   // convenience getter for easy access to form fields
  get a() { return this.descriptionForm.controls; }
  get b() { return this.errorForm.controls; }
  get c() { return this.resolutionForm.controls; }
  
 

textArea(){
  console.log("Agent description: " +this.description );
}


selectDepartment(department: string) {
    this.departments = department;
    console.log(this.departments);
    console.log(this.badge);
  }

  selectResolution(resolution: string) {
    this.resolution = resolution;
    console.log(this.resolution);
  }
    selectRestocking(restValue: string) {
    this.restocking = restValue;
    console.log(this.restocking);
  }

     selectSts(sts: string) {
    this.sts = sts; 
    console.log(this.sts);
  }

    //test
  fsrOption(fsr: string) {
  this.fsrNumber = fsr;
  console.log(this.fsrNumber);
}
getFsr(value:string){
  this.fsrValue = value;
if (this.fsrValue == undefined || this.fsrValue == '') {
  this.fsrNumber = "N/A";
} else {
  this.fsrNumber = this.fsrValue;
  console.log(this.fsrNumber);
}
}

  oopOption(oppValue: string): void {
    if (!oppValue == undefined || oppValue == '') {
      this.oopDetails = "";     
       } else {
      this.oopDetails = `****OOP/Large Dollar Details****\n${oppValue}`;     
      console.log(this.oopDetails);
    }

  }

  submitForm(){  
    this.recordTime  = new Date();
  if(this.oopDetails == undefined ||this.oopDetails == ''){
    this.oopRecord = 'NO';
  } else {
    this.oopRecord = 'YES';
  }
 
   
      this.goodText = `Detailed description of customer issue: ${this.description} \nWho caused the error: ${this.departments} \nHow it is being resolved: ${this.resolution} \nSTS (Save the Sale):  ${this.sts} \nRSF (Re-Stocking Fee):  ${this.restocking}\nFSR(Finance Service Request):  ${this.fsrNumber} \n\n${this.oopDetails}\n`;
      this.clipboard = new Clipboard('#cpyBtn'); 
      this.recordService.postRecord(this.newRecord ={
       recordTime: this.recordTime,
        badge: this.badge,
        description:this.description,
        department:this.departments,
        resolution:this.resolution,
        oop:this.oopRecord

      }).subscribe(

        res => {
          console.log(this.newRecord);
        },
        err =>{
          console.log(err);
        },
      );

    console.log(this.goodText);   
   
  }
  submitOop(){  
    this.submitted = true;
    this.title = "Good Text";
    this.recordTime  = new Date();
    console.log(this.recordTime);

    if(this.oopDetails == undefined ||this.oopDetails == ''){
      this.oopRecord = 'NO';
    } else {
      this.oopRecord = 'YES';
    }

    this.goodText = `Detailed description of customer issue: ${this.description} \nWho caused the error: ${this.departments} \nHow it is being resolved: ${this.resolution} \nSTS (Save the Sale):  ${this.sts} \nRSF (Re-Stocking Fee):  ${this.restocking}\nFSR(Finance Service Request):  ${this.fsrNumber} \n\n${this.oopDetails}\n`;
      this.clipboard = new Clipboard('#cpyBtn');
      this.goodTextSlider.lockSwipes(false);
      this.goodTextSlider.slidePrev();
      this.goodTextSlider.lockSwipes(true);
      this.recordService.postRecord(this.newRecord ={
        recordTime: this.recordTime,
         badge: this.badge,
         description:this.description,
         department:this.departments,
         resolution:this.resolution,
         oop:this.oopRecord
 
       }).subscribe(
 
         res => {
           console.log(this.newRecord);
         },
         err =>{
           console.log(err);
         },
       );
 
      console.log(this.goodText);       
  }

    clearAll(){   
      this.stsForm.reset();
      this.restockingForm.reset();
      this.fsrForm.reset();            
      this.descriptionForm.controls['text'].reset(''); 
      this.oopForm.controls['oopText'].reset('');     
      this.errorForm.reset({
        department: ['', Validators.required]
      });
      this.resolutionForm.reset({
        action: ['', Validators.required]
      });
      this.sts = "N/A"
      this.restocking = "N/A";
      //this.fsrValue = "N/A"; 
      this.oopDetails = "";
      //this.ngOnInit();

  }

  goToSlideTwo() {
   this.title = "OOP && Large Dollar Details";

    this.goodTextSlider.lockSwipes(false);
    this.goodTextSlider.slideNext();
    this.goodTextSlider.lockSwipes(true);
  }

  goBack(){
    this.title = "Good Text";
    this.goodTextSlider.lockSwipes(false);
    this.goodTextSlider.slidePrev();
    this.goodTextSlider.lockSwipes(true);
}
  
ClearOOP(){
  this.oopForm.controls['oopText'].reset('');
}
addOopTemplate(){
  this.oopForm.controls['oopText'].setValue(this.oopTemplate);
}

addLargeDollarTemplate(){
  this.oopForm.controls['oopText'].setValue(this.largeDollarTemplate);
}
}
