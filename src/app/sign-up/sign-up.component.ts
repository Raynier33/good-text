import { Component, OnInit } from '@angular/core';
import { UserService } from "../shared/user/user.service";
import { Router, NavigationExtras  } from "@angular/router";
import { NgForm } from '@angular/forms';
import { HomePage } from "../home/home.page";



@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss'],
  providers:[UserService]
})
export class SignUpComponent implements OnInit {

  title = "Please enter your badge";
  showSucessMessage: boolean;
serverErrorMessages: string;

  constructor(public userService: UserService, public router: Router) { }

  ngOnInit() {
  }

  onSubmit(form: NgForm) {

  

    //save user
    this.userService.postUser(form.value).subscribe(
      res => {
        this.showSucessMessage = true;
        setTimeout(() => this.showSucessMessage = false, 4000);
        this.resetForm(form);
        
      },
      err => {
        if (err.status === 422) {
          this.serverErrorMessages = err.error.join('<br/>');
        }
        else
          this.serverErrorMessages = 'Something went wrong.Please contact admin.';
          console.log(err);
      }
      
    );
      //pass data to next page
      let navigationExtras: NavigationExtras  = {
        state: {
          agentId: form.value
          }       
      };
    this.router.navigate(['home'], navigationExtras ); 
    console.log(navigationExtras);
   }
    


  resetForm(form: NgForm) {
    this.userService.selectedUser = {
      badge: ''
    };
    form.resetForm();
    this.serverErrorMessages = '';
  }
}
