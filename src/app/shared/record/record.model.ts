export class Record {
    recordTime:string;
    badge:string;
    description: string;
    department: string;
    resolution:string;
    oop:any;
}