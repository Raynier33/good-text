import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Record } from "./record.model";

@Injectable({
  providedIn: 'root'
})
export class RecordService {

  selectedRecord: Record = {
    recordTime:'',
    badge:'',
    description: '',
    department:'',
    resolution:'',
    oop:''
  };

  constructor(public http: HttpClient) { }

  postRecord(record: Record){
    return this.http.post(environment.apiBaseUrl+'/addRegistry',record);
  }
}
