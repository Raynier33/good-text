
const express = require('express');
const router = express.Router();

const ctrlUser = require('../controllers/user.controller');
const ctrlRecord = require('../controllers/record.controller');

router.post('/register', ctrlUser.register);
router.post('/addRegistry', ctrlRecord.addRegistry);

module.exports = router;