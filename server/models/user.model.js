const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');

 
var userSchema = new mongoose.Schema({
   
    badge: {
        type: String,
        required: 'badge can\'t be empty',
        unique: true
    }    

});


mongoose.model('User', userSchema);