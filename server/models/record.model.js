const mongoose = require('mongoose');
 
var recordSchema = new mongoose.Schema({

    recordTime: {
        type: String,
        required: 'time can\'t be empty',
        unique: true
    },
    badge: {
        type: String,
        required: 'badge can\'t be empty',
    },   
    description: {
        type: String,
        required: 'description can\'t be empty'
       },   
    department: {
        type: String,
        required: 'department can\'t be empty'        
    },  
    resolution: {
        type: String,
        required: 'resolution can\'t be empty'        
    },
    oop: {
        type: String,      
    }

});


mongoose.model('Record', recordSchema);